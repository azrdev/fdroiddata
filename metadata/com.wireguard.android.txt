Categories:Security,System,Connectivity,Internet
License:GPL-2.0+
Web Site:https://www.wireguard.com/
Source Code:https://git.zx2c4.com/wireguard-android/
Issue Tracker:https://lists.zx2c4.com/mailman/listinfo/wireguard
Donate:https://www.wireguard.com/donations/
Bitcoin:1ASnTs4UjXKR8tHnLi9yG42n42hbFYV2um

Auto Name:WireGuard
Summary:Next generation secure VPN network tunnel
Description:
WireGuard is a next generation secure network tunnel protocol. This application
allows users to connect to WireGuard tunnels. More information may be found on
the [project webpage https://www.wireguard.com].

If your device has a custom kernel containing the WireGuard module, then the
module will be used for superior battery life and performance. Otherwise a
userspace version will work sufficiently on all other devices.
.

Repo Type:git
Repo:https://git.zx2c4.com/wireguard-android/

Build:0.4.0,405
    commit=0.4.0
    subdir=app
    submodules=yes
    gradle=yes
    ndk=r16b

Build:0.4.1,406
    commit=0.4.1
    subdir=app
    submodules=yes
    gradle=yes
    ndk=r16b

Build:0.4.2,407
    commit=0.4.2
    subdir=app
    submodules=yes
    gradle=yes
    ndk=r16b

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.4.2
Current Version Code:407
